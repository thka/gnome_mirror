# gnome_mirror


## Description

Python script for downloading selected or all gnome extensions from https://extensions.gnome.org/

Python script for a gnome extensions server for use when an internet connection is non-existent.

## Getting started

Add the extensions in the configuration.json

```
./mirror_gnome_extensions.py
```

## Publish the extensions internally

Start the server

```
./server_gnome_extensions.py
```

## License
GNU GPL
