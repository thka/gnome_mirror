#!/usr/bin/python3

# Mirror Gnome extensions
# Copyright 2023 Thomas Karlsson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# e-post thomas.karlsson@relea.se

import argparse
from typing import Dict, List, Any, TypedDict
import requests
import json
import os
from semantic_version import Version  # type: ignore
import re
import datetime
from dateutil.parser import parse as parsedate


class Gnome_extension:
    def __init__(self, extension: Dict[str, Any], save_directory: str = ''):
        self.extension = extension
        self.base_url = 'https://extensions.gnome.org'
        self.save_directory = save_directory
        self.uuid = self.extension.get('uuid', '')
        self.name = self.extension.get('name', '')
        self.creator = self.extension.get('creator', '')
        self.creator_url = self.extension.get('creator_url', '')
        self.pk = self.extension.get('pk', 0)
        self.description = self.extension.get('description', '')
        self.link = self.extension.get('link', '')
        self.icon = self.extension.get('icon', '')
        self.screenshot = self.extension.get('screenshot', '')
        self.downloads = self.extension.get('downloads', 0)
        self.shell_version_map = self.extension.get('shell_version_map', dict())

    def __getitem__(self, item):
        return self.extension[item]

    def save_filename(self, version: int = 0) -> str:
        # user-theme@gnome-shell-extensions.gcampax.github.com.v49.shell-extension.zip
        return f'{self.uuid}.v{version}.shell-extension.zip'

    def extension_url(self, pk: int = 0):
        """
        /download-extension/hide-dash@zacbarton.com.shell-extension.zip?version_tag=1993
        """
        return '/download-extension/' + self.uuid + '.shell-extension.zip?version_tag=' + str(pk)

    def download_url(self, pk: int = 0):
        """
        https://extensions.gnome.org/download-extension/hide-dash@zacbarton.com.shell-extension.zip?version_tag=1993
        """
        return self.base_url + '/download-extension/' + self.uuid + '.shell-extension.zip?version_tag=' + str(pk)

    def comments_url(self, pk: int = 0, all: bool = False):
        """
        https://extensions.gnome.org/comments/all/?pk=36&all=false
        """
        return f'{self.base_url}/comments/all/?pk={self.pk}&all={str(all).lower()}'

    def server_download_url(self, shell_version: str):
        """
        https://extensions.gnome.org/download-extension/hide-dash@zacbarton.com.shell-extension.zip?version_tag=1993
        """
        pk = self.shell_version_to_pk(shell_version)
        return '/download-extension/' + self.uuid + '.shell-extension.zip?version_tag=' + str(pk)

    def static_download_url(self, shell_version: str):
        """
        https://extensions.gnome.org//extension-data/user-themegnome-shell-extensions.gcampax.github.com.v49.shell-extension.zip
        """
        version = self.shell_version_to_version(shell_version)

        return f'/extension-data/{self.uuid}.v{version}.shell-extension.zip'

    def screenshot_url(self):
        """
        https://extensions.gnome.org/extension-data/screenshots/screenshot_28.png
        """
        return self.base_url + self.screenshot

    def link_url(self):
        """
        https://extensions.gnome.org/extension/28/gtile/
        """
        return self.base_url + self.link

    def icon_url(self):
        """
        https://extensions.gnome.org/extension-data/icons/icon_28.png
        """
        return self.base_url + self.icon

    def extension_info_url(self):
        """
        https://extensions.gnome.org/extension-info/?uuid=Move_Clock@rmy.pobox.com
        """
        return self.base_url + '/extension-info' + f'/?uuid={self.uuid}'

    def download(self, pk: int = 0, save_path: str = '') -> int:
        request = requests.get(self.download_url(pk), stream=True)
        with open(save_path, 'wb') as filehandle:
            for block in request.iter_content(1024):
                filehandle.write(block)
        if request.status_code == 200:
            return 200

        return request.status_code

    def download_extension(self, pk: int = 0, save_path: str = '') -> int:
        return self.download_object(self.download_url(pk), save_path)

    def download_screenshot(self, save_path: str = '') -> int:
        return self.download_object(self.screenshot_url(), save_path)

    def download_comments(self, save_path: str = '', all: bool = False) -> int:
        return self.download_object(self.comments_url(all=all), save_path)

    def download_icon(self, save_path: str = '') -> int:
        return self.download_object(self.icon_url(), save_path)

    def download_info(self, save_path: str = '') -> int:
        'https://extensions.gnome.org/extension-info/?uuid=Move_Clock@rmy.pobox.com'
        url = f'https://extensions.gnome.org/extension-info/?uuid={self.uuid()}'
        print("Fetching info for", url)

        return self.download_object(self.extension_info_url(), save_path)

    def download_object(self, remote_url: str = '', save_path: str = '') -> int:
        request = requests.get(remote_url, stream=True)
        with open(save_path, 'wb') as filehandle:
            for block in request.iter_content(1024):
                filehandle.write(block)
        if request.status_code == 200:
            return 200

        return request.status_code

    def remove_shell_version(self, version: str):
        if version in self.shell_versions():
            del self.shell_version_map[version]

    def filter_shell_versions(self, versions_to_keep: List[str]) -> List[str]:
        """
        versions_to_keep: A list with versions to keep
        """
        for one_version in self.shell_versions():
            if one_version not in versions_to_keep:
                self.remove_shell_version(one_version)

        return self.shell_versions()

    def shell_versions(self) -> List[str]:
        return list(self.shell_version_map.keys())

    def shell_version(self, version: str) -> Dict[str, str]:
        if version not in self.shell_versions():
            return dict()

        return self.shell_version_map[version]

    def pk_version(self, shell_version: str) -> int:
        if shell_version not in self.shell_versions():
            return 0

        if 'pk' not in self.shell_version(shell_version):
            return 0

        return int(self.shell_version(shell_version)['pk'])

    def shell_version_to_pk(self, shell_version: str) -> int:
        """Find out the pk_version from a shell version

        shell_version
        return if 40.5 doesn't exist, it returns the nearest
        """
        if shell_version in self.shell_versions():
            return self.pk_version(shell_version)
        for position in range(1, len(shell_version.split('.'))):
            if '.'.join(shell_version.split('.')[:-position]) in self.shell_versions():
                return self.pk_version('.'.join(shell_version.split('.')[:-position]))

        return 0

    def shell_version_to_version(self, shell_version: str) -> int:
        """Find out the version from a shell version

        shell_version
        return if 40.5 doesn't exist, it returns the nearest
        """
        if shell_version in self.shell_versions():
            return self.pk_version(shell_version)
        for position in range(1, len(shell_version.split('.'))):
            if '.'.join(shell_version.split('.')[:-position]) in self.shell_versions():
                return self.version_version('.'.join(shell_version.split('.')[:-position]))

        return 0

    def version_version(self, shell_version: str) -> int:
        if shell_version not in self.shell_versions():
            return 0

        if 'version' not in self.shell_version(shell_version):
            return 0

        return int(self.shell_version(shell_version)['version'])

    def text(self) -> str:
        return json.dumps(self.extension)

    def extension_info(self, shell_version: str) -> str:
        extra_info = {}
        extra_info['version'] = self.shell_version_to_version(shell_version)
        extra_info['version_tag'] = self.shell_version_to_pk(shell_version)
        extra_info['download_url'] = self.server_download_url(shell_version)
        self.extension.update(extra_info)

        return json.dumps(self.extension)

    def dictionary(self) -> Dict[str, Any]:
        return self.extension

    def compareversions(self, comparestr: str, versionone, versiontwo) -> bool:
        if re.search(r'[^\d\.]+', versionone):
            return False
        if re.search(r'[^\d\.]+', versiontwo):
            return False
        # print(f'Comparing {versionone} {comparestr} {versiontwo}')
        if comparestr == '==':
            if Version(self._converttosemversion(versionone)) == Version(self._converttosemversion(versiontwo)):
                return True
        elif comparestr == '>=':
            if Version(self._converttosemversion(versionone)) >= Version(self._converttosemversion(versiontwo)):
                return True
        elif comparestr == '<=':
            if Version(self._converttosemversion(versionone)) <= Version(self._converttosemversion(versiontwo)):
                return True
        elif comparestr == '>':
            if Version(self._converttosemversion(versionone)) > Version(self._converttosemversion(versiontwo)):
                return True
        elif comparestr == '<':
            if Version(self._converttosemversion(versionone)) < Version(self._converttosemversion(versiontwo)):
                return True

        return False

    def listvalidshellversions(self, configuredversions: List[str]) -> List[str]:
        validversions = []
        comparestring = ''
        for oneversion in self.shell_versions():
            vers = ''
            for oneconfigversion in configuredversions:
                compareobject = re.search('^([<>=]+)', oneconfigversion)
                if compareobject:
                    comparestring = compareobject[1]
                    versionobject = re.search('^([<>=]+)([0-9.]+)', oneconfigversion)
                    vers = versionobject.group(2)
                else:
                    comparestring = '=='
                    versionobject = re.search('([0-9.]+)', oneconfigversion)
                    vers = versionobject.group(1)
                if self.compareversions(comparestring,
                                        self._converttosemversion(oneversion),
                                        self._converttosemversion(vers)):
                    if oneconfigversion not in validversions:
                        validversions.append(oneversion)

        return validversions

    def _converttosemversion(self, inversion: str) -> str:
        if inversion.count('.') == 1:
            return inversion + ".0"
        elif inversion.count('.') == 2:
            return inversion
        elif inversion.count('.') == 0:
            return inversion + '.0.0'
        else:
            return inversion


class Gnome_extensions:
    def __init__(self, extensionsfile: str):
        self.rawdata: Dict[str, Any] = dict()
        self.extensionsfile = extensionsfile
        self.extensions: List[Gnome_extension] = []

    def load(self) -> bool:
        if not os.path.exists(self.extensionsfile):
            raise FileNotFoundError

        with open(self.extensionsfile, 'rt') as extfile:
            self.rawdata = json.loads(extfile.read())
            self._parse_extensions()

            return True

        return False

    def downloadindex(self, forcedownload: bool = False) -> int:
        'https://extensions.gnome.org/static/extensions.json'
        url = 'https://extensions.gnome.org/static/extensions.json'
        request = requests.head(url)
        url_datetime = parsedate(request.headers['Last-Modified']).astimezone()
        url_time = datetime.datetime.fromisoformat(str(url_datetime))
        file_time = datetime.datetime.fromisoformat('1980-01-01 00:00:00+00:00')
        if os.path.exists(self.extensionsfile):
            file_time = datetime.datetime.fromtimestamp(
                os.path.getmtime(
                    self.extensionsfile
                )).astimezone()
        if file_time == 0:
            forcedownload = True
        if forcedownload or url_time > file_time:
            print("[INFO] Fetching index", url)
            request = requests.get(url, stream=True)
            with open(self.extensionsfile, 'wb') as filehandle:
                for block in request.iter_content(1024):
                    filehandle.write(block)
            # if request.status_code != 200:
            return request.status_code

        return 200

    def _parse_extensions(self):
        for one_extension in self.rawdata['extensions']:
            self.extensions.append(Gnome_extension(one_extension))

    def by_name(self, extension_name: str) -> Gnome_extension:
        for one in self.extensions:
            if one.name == extension_name:
                return one

        return Gnome_extension(dict())

    def by_uuid(self, extension_uuid: str) -> Gnome_extension:
        for one in self.extensions:
            if one.uuid == extension_uuid:
                return one

        return Gnome_extension(dict())

    def list(self) -> List[Gnome_extension]:
        return self.extensions

    def search(self, searchterm: str, order: str = 'name', limit: int = 20) -> List[Gnome_extension]:
        matched_extensions: List[Gnome_extension] = []
        search_lower = searchterm.lower()
        for one_extension in self.list():
            if search_lower in one_extension.name.lower():
                matched_extensions.append(one_extension)
            elif search_lower in one_extension.creator.lower():
                matched_extensions.append(one_extension)
            elif search_lower in one_extension.description.lower():
                matched_extensions.append(one_extension)

        if order.lower() == 'name':
            newlist = sorted(matched_extensions, key=lambda d: d['name'])
        if order.lower() == 'popularity' or order.lower() == 'downloads':
            newlist = sorted(matched_extensions, key=lambda d: d['downloads'], reverse=True)

        return newlist[:limit]

    def export_to_html(self) -> str:
        html = str()
        html += "<html><header>Gnome extensions</header><body>\n"
        html += "<table border=\"1\">\n"
        for extension in self.list():
            html += "<tr><td>" + extension.name + "</td>"
            html += "<td>" + "<img src=\"" + extension.screenshot + "\"></td>"
            html += "</tr>\n"
        html += "</table>\n"
        html += "</body></html>\n"

        return html
