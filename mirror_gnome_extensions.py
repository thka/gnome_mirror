#!/usr/bin/python3

# Mirror Gnome extensions
# Copyright 2023 Thomas Karlsson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# e-post thomas.karlsson@relea.se

import argparse
import json
import os
# from semantic_version import Version  # type: ignore
from typing import Dict, Any
# from dateutil.parser import parse as parsedate
from gnomeconfig import Configuration
from gnomeextensions import Gnome_extensions


def write_mirrored_index(basedirectory: str, index: Dict[str, Any]):
    with open(os.path.join(basedirectory, 'extensions.json'), 'wt') as fh:
        fh.write(json.dumps(index))


def main():
    parser = argparse.ArgumentParser(description='Mirror Gnome extensions')
    parser.add_argument('-c',
                        '--config',
                        dest='config',
                        action='store',
                        help='Configuration file',
                        default='configuration.json',
                        required=False)
    parser.add_argument('-d',
                        '--download_all',
                        dest='download_all',
                        action='store_true',
                        help='Download all extensions',
                        required=False)
    args = parser.parse_args()

    configuration = Configuration(args.config)

    mirroredindex = {}
    mirroredextensions = []
    extensionindex = Gnome_extensions(os.path.join(configuration.base_directory, 'extensions.json'))
    _ = extensionindex.downloadindex()
    extensionindex.load()
    non_saved_counter = 0
    for onlineextension in extensionindex.list():
        if onlineextension.uuid in configuration.extensions() or args.download_all:
            for oneversion in onlineextension.listvalidshellversions(configuration.shell_versions):
                # save_path = os.path.join(configuration.base_directory, onlineextension.save_filename(onlineextension.pk_version(oneversion)))
                save_path = os.path.join(configuration.base_directory, onlineextension.save_filename(onlineextension.version_version(oneversion)))
                if not os.path.exists(save_path):
                    statuscode = onlineextension.download_extension(onlineextension.pk_version(oneversion), save_path)
                    if statuscode == 200:
                        non_saved_counter += 1
                        print(f'Downloaded {onlineextension.uuid} for Gnome {oneversion}')

            if configuration.screenshots:
                if onlineextension.screenshot is not None:
                    screenshot_filename = configuration.base_directory + onlineextension.screenshot
                    if not os.path.exists(screenshot_filename):
                        statuscode = onlineextension.download_screenshot(screenshot_filename)
                        if statuscode == 200:
                            print(f'Downloaded screenshot for {onlineextension.name}')
            if configuration.icons:
                if onlineextension.icon is not None:
                    icon_filename = configuration.base_directory + onlineextension.icon
                    if not os.path.exists(icon_filename):
                        statuscode = onlineextension.download_icon(icon_filename)
                        if statuscode == 200:
                            print(f'Downloaded icon for {onlineextension.name}')
            if configuration.comments:
                comments_filename = os.path.join(configuration.base_directory, f'comments/all/comments-{onlineextension.pk}')
                if not os.path.exists(comments_filename):
                    statuscode = onlineextension.download_comments(comments_filename, all=True)
                    if statuscode == 200:
                        print(f'Downloaded comments for {onlineextension.name}')

            # Remove non wanted shell versions
            wanted_shell_versions = onlineextension.listvalidshellversions(configuration.shell_versions)
            _ = onlineextension.filter_shell_versions(wanted_shell_versions)

            mirroredextensions.append(onlineextension.dictionary())

        if non_saved_counter >= 5:
            mirroredindex['extensions'] = mirroredextensions
            write_mirrored_index(configuration.static_directory, mirroredindex)
            non_saved_counter = 0

    mirroredindex['extensions'] = mirroredextensions
    write_mirrored_index(configuration.static_directory, mirroredindex)
    # print('Saved extension index')
    # mirrored_extensions = Gnome_extensions(os.path.join(configuration.static_directory, 'extensions.json'))

    # with open('index.html', 'wt') as htmlfile:
    #    # htmlfile.write(extensionindex.export_to_html())
    #    htmlfile.write(create_html(mirrored_extensions))


if __name__ == "__main__":
    main()
