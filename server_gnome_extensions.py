#!/usr/bin/python3

# Mirror Gnome extensions
# Copyright 2023 Thomas Karlsson
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License forxi
# more details.
#
# You should have received a copy of the GNU General Public License along with this
# program. If not, see <https://www.gnu.org/licenses/>.
#
# e-post thomas.karlsson@relea.se

import argparse
import os
# import sys
import ssl
import json
from gnomeconfig import Configuration
from gnomeextensions import Gnome_extensions
from typing import List, Any, Dict
import tornado.ioloop
import tornado.web
import logging


logging.basicConfig(
    level=getattr(logging, "INFO"),
    format="[%(levelname)s %(asctime)s path:%(pathname)s lineno:%(lineno)s] %(message)s",  # noqa
    datefmt="%Y/%m/%d %I:%M:%S"
)

configuration: Configuration = Configuration('', load=False)
extensionindex = Gnome_extensions('downloaded_extensions/static/extensions.json')
extensionindex.load()


class ExtensionInfoHandler(tornado.web.RequestHandler):
    def get(self):
        shell_version = self.get_argument('shell_version', None, True)
        uuid = self.get_argument('uuid', None, True)
        current_extension = extensionindex.by_uuid(uuid)

        self.set_header('Content-Type', 'application/json')
        if shell_version is not None:
            self.write(current_extension.extension_info(shell_version))
        else:
            self.write(current_extension.text())


class ExtensionQueryHandler(tornado.web.RequestHandler):
    def get(self):
        searchterm = self.get_argument('search', None, True)
        sorting = self.get_argument('sort', 'popularity', True)
        matched = extensionindex.search(searchterm, sorting)
        return_list: List[Dict[str, Any]] = []
        for one in matched:
            return_list.append(one.dictionary())

        self.set_header('Content-Type', 'application/json')
        self.write(json.dumps({'extensions': return_list}))


class CommentsHandler(tornado.web.RequestHandler):
    def get(self):
        pk = self.get_argument('pk', None, True)
        all_comments = self.get_argument('all', None, True)
        self.set_header('Content-Type', 'application/json')
        comments_path = os.path.join(configuration.comments_directory, 'all', f'comments-{pk}')
        # self.set_header('X-debug', comments_path)
        if os.path.exists(comments_path):
            with open(comments_path, 'r') as commentsfh:
                if all_comments:
                    self.write(commentsfh.read())
                else:
                    comments = json.load(commentsfh)
                    no_comments = len(comments)
                    if no_comments >= 5:
                        self.write(json.dumps(comments[:5]))
                    else:
                        self.write(json.dumps(comments[:no_comments]))
        else:
            self.write('[]')


class ExtensionDownloadInfoHandler(tornado.web.RequestHandler):
    def get(self, uuid: str):
        wanted_shell_version = str(self.get_argument('shell_version', None, True))
        wanted_extension = extensionindex.by_uuid(uuid)
        self.set_status(302, reason='Found')
        self.set_header('Location', wanted_extension.static_download_url(wanted_shell_version))
        self.finish()


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("<html><header>Gnome extensions</header><body>\n")
        self.write("<table border=\"1\">\n")
        for extension in extensionindex.list():
            self.write(f'<tr><td><a href="{extension.link}">{extension.name}</a></td></tr>\n')
        self.write("</table>\n")
        self.write("</body></html>")


def make_app():
    endpoints = [
        (r"/", MainHandler),
        # (r"/extension/(\d+)/(.*)", ExtensionHandler),
        (r"/extension-query/.*", ExtensionQueryHandler),
        (r"/extension-info/.*", ExtensionInfoHandler),
        (r"//?extension-data/(.*)", tornado.web.StaticFileHandler, {'path': configuration.extension_data_directory}),
        (r"/static/(extensions.json)", tornado.web.StaticFileHandler, {'path': configuration.static_directory}),
        (r"/comments/all/", CommentsHandler),
        (r"/download-extension/(.*).shell-extension.zip.*", ExtensionDownloadInfoHandler)
    ]
    return tornado.web.Application(endpoints, debug=True)


def servername_callback(sock, hostname, cb_context):
    # hostname contains the hostname that the client is requesting
    # now that we have the hostname we can dynamically pick the correct certificate
    ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
    # this part is up to you to store via a config file or even in a database
    # ssl_context.load_cert_chain(certfile='/home/thka/develop/extensions.gnome.org/extensions.gnome.org.crt', keyfile='/home/thka/develop/extensions.gnome.org/extensions.gnome.org.key')
    ssl_context.load_cert_chain(certfile=configuration.tls_certificate,
                                keyfile=configuration.tls_key)

    sock.context = ssl_context


def main():
    parser = argparse.ArgumentParser(description='Mirror Gnome extensions')
    parser.add_argument('-c',
                        '--config',
                        dest='config',
                        action='store',
                        help='Configuration file',
                        default='configuration.json',
                        required=False)
    args = parser.parse_args()

    configuration = Configuration(args.config)

    app = make_app()
    app.listen(configuration.http_port)
    logging.info(f'Listening on {configuration.http_port} for HTTP')

    ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
    ssl_context.minimum_version = ssl.TLSVersion.TLSv1_2
    ssl_context.maximum_version = ssl.TLSVersion.TLSv1_3
    # ssl_context.sni_callback = servername_callback
    ssl_context.load_cert_chain(certfile=configuration.tls_certificate,
                                keyfile=configuration.tls_key)

    apps = make_app()
    https_server = tornado.httpserver.HTTPServer(apps, ssl_options=ssl_context)
    https_server.listen(configuration.https_port)
    logging.info(f'Listening on {configuration.https_port} for HTTPS')
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
