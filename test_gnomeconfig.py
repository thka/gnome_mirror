#!/usr/bin/python3

from gnomeconfig import Configuration


def test_configuration():
    testconfig = Configuration('testconfig.json')
    assert testconfig.base_directory == 'downloaded_extensions'
    assert testconfig.static_directory == 'downloaded_extensions/static'
    assert testconfig.extensions == ["hide-dash@zacbarton.com",
                                     "calc@patapon.info",
                                     "system-monitor@paradoxxx.zero.gmail.com",
                                     "focusfollowsmouse@dlachausse",
                                     "panel-docklet@quina.at",
                                     "gTile@vibou"]
    assert testconfig.extension_configured('user-theme@asdfasdf') is False
    assert testconfig.extension_configured('panel-docklet@quina.at') is True

    missingbasedir = Configuration('testconfig_missing_basedirectory.json')
    assert missingbasedir.base_directory == ''
