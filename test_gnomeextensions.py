#!/usr/bin/env python3

from gnomeextensions import Gnome_extensions


def test_gnome_extensions():
    extensionindex = Gnome_extensions('tests/extensions.json')
    assert extensionindex.load() is True
    assert len(extensionindex.list()) == 1000
    assert len(extensionindex.search('focus')) == 22

def test_gnome_extension():
    extensionindex = Gnome_extensions('tests/extensions.json')
    assert extensionindex.load() is True
    extension = extensionindex.by_uuid('containers@royg')
    assert extension.name == 'Containers'
    assert extension.downloads == 7768
    assert extension.uuid == 'containers@royg'
    assert extension.name == 'Containers'
    assert extension.creator == 'royg'
    assert extension.creator_url == '/accounts/profile/royg'
    assert extension.pk == 1500
    assert extension.description == 'Manage podman containers through a gnome-shell menu'
    assert extension.link == '/extension/1500/containers/'
    assert extension.icon == '/extension-data/icons/icon_1500.png'
    assert extension.screenshot == '/extension-data/screenshots/screenshot_1500_DAvFcEj.png'

    assert extension.shell_versions() == ["3.28.3", "3.32.2", "3.34.1", "3.34", "3.36", "40", "41", "42", "43", "44"]
    assert extension.shell_version_to_pk('3.34') == 26705
    assert extension.shell_version_to_pk('3.34.1') == 13402
    assert extension.pk_version('3.34.1') == 13402
    assert extension.version_version('3.34.1') == 9
