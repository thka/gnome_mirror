#!/usr/bin/python3

from gnomeextensions import Gnome_extensions


def test_gnome_extension():
    extensions = Gnome_extensions('test_extensions.json')
    ext = extensions.by_name("Frippery Move Clock")
    assert ext.uuid == "Move_Clock@rmy.pobox.com"
    assert ext.creator == "rmyorston"
    assert ext.creator_url == "/accounts/profile/rmyorston"
    assert ext.description == "Move clock to left of status menu button"
    assert ext.link == "/extension/2/move-clock/"
    assert ext.link_url() == "https://extensions.gnome.org/extension/2/move-clock/"
    assert ext.icon == "/extension-data/icons/icon_2.png"
    assert ext.icon_url() == "https://extensions.gnome.org/extension-data/icons/icon_2.png"
    assert ext.screenshot == "/extension-data/screenshots/screenshot_2.png"
    assert ext.download_url(1059) == 'https://extensions.gnome.org/download-extension/Move_Clock@rmy.pobox.com.shell-extension.zip?version_tag=1059'
    ext = extensions.by_uuid("Shut_Down_Menu@rmy.pobox.com")
    assert ext.shell_versions() == ['3.2', '3.4', '3.6', '3.8']
    assert ext.shell_version('3.6') == {'pk': 2265, 'version': 7}
    assert ext.pk_version('3.6') == 2265
    assert ext.version_version('3.6') == 7
    ext.remove_shell_version('3.4')
    assert ext.shell_versions() == ['3.2', '3.6', '3.8']
    assert ext.filter_shell_versions(['3.6', '3.8']) == ['3.6', '3.8']
    assert ext.shell_versions() == ['3.6', '3.8']

    pk = extensions.by_pk(28)
    assert pk.name == 'gTile'
